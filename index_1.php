<?php

include 'php/bibli_gazette.php';
include 'php/bibli_generale.php';

$pseudo = $_POST['utPseudo'];

eh_toutDebut('./styles/gazette.css');

eh_afficherDebut('Le site de désinformation n°1 des étudiants en Licence Info',$pseudo,".");

//-----------

$a1 = new Article(10,'Un mouchard dans un corrigé de Langages du Web','hacker.jpg');
$a2 = new Article(9,'Votez pour l\'hymne de la Licence','hymne.jpg');
$a3 = new Article(8,'L\'amphi Sciences Naturelles bientôt renommé amphi Mélenchon','melenchon.jpg');

$bloque = new Block('&Agrave; la Une','centre');
$bloque->afficher($a1,$a2,$a3);

//-----------

$a1->load(1,'Résultat de notre sondage : allez-vous réussir votre année ?','sondage.jpg');
$a2->load(7,'Toute une famille de pingouins découverte dans l\'amphi B','pingouins.jpg');
$a3->load(3,' Macron obtient sa Licence Info en EAD','macron.jpg');

$bloque->load('L\'info brûlante','centre');
$bloque->afficher($a1,$a2,$a3);

//-----------

$a1->load(2,'Il avait annoncé \'Je vais vous défoncer\' l\'enseignant relaxé','walkingdead.jpg');
$a3->load(4,'Donald Trump veut importer les CMI aux Etats-Unis','trump.jpg');
$a2->load(5,'Le calendier des Dieux de la Licence bientôt disponible','calendrier.jpg');

$bloque->load('Les incontournables','centre');
$bloque->afficher($a1,$a2,$a3);

//-----------

eh_astro();

eh_Fin();
?>
