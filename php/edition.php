<?php 
ob_start();
error_reporting(E_ALL);
session_start();

include 'bibli_gazette.php';
include 'bibli_generale.php';

if ($_SESSION['statut']!=1 && $_SESSION['statut']!=2 && $_SESSION['statut']!=3) {
    header("Location: ../index.php");
    die();
}

$B=eh_bd_connecter();

foreach ($_POST as &$value) {
    $value = htmlspecialchars(htmlentities($value));
}

eh_toutDebut('../styles/gazette.css');
eh_afficherDebut("Modification Article","..");

if ($_POST['arid']!=NULL) {

    $S1 = 'SELECT arAuteur FROM article WHERE arID = '.$_POST['arid'];
    $R1 = mysqli_query($B, $S1) or eh_bd_erreur($B,$S1);
    $T1 = mysqli_fetch_assoc($R1);

    if (strcmp($_SESSION['pseudo'],$T1['arAuteur'])!=0) {
        echo '<section><h2>Erreur</h2>Seul l\'auteur de l\'article peut le modifier</section>';
    }else{

        $S = 'SELECT arTitre,arResume,arTexte FROM article WHERE arID ='.$_POST['arid'];
        $R = mysqli_query($B, $S);
        $T = mysqli_fetch_assoc($R);


        echo '<section><h2>Nouvel Article</h2>';
        echo'<form action="../php/edition.php?id='.$_POST['arid'].'" method="post">
            <table>
                <tr>
                    <td><label for="titre">Choisissez un titre :</label></td>
                    <td><input type="text" name="titre" id="titre" value="',$T['arTitre'],'"></td>
                </tr>
                <tr>
                    <td><label for="resume">Quel est le résumé :</label></td>
                    <td><textarea name="resume" rows="10" cols="80">',$T['arResume'],'</textarea></td>
                </tr>
                <tr>
                    <td><label for="texte">Votre texte :</label></td>
                    <td><textarea name="texte" rows="30" cols="100">',$T['arTexte'],'</textarea></td>
                </tr>
                <tr>
                    <td>Supprimer l\'article ?</td>
                    <td><input type="checkbox" name="supp" value="1"></td>
                    </tr>
                <tr>
                    <td colspan="2">
                        <input type="submit" name="ok" value="Modifier">
                        <input type="reset" value="Réinitialiser">
                    </td>
                </tr>
            </table>';
        echo '</section>';
    }
}else{
    echo '<section><h2>Nouvel Article</h2>';
    echo'<form action="../php/edition.php" method="post">
            <table>
                <tr>
                    <td><label for="arid">Choisissez un article :</label></td>
                    <td><input type="text" name="arid" value=""></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <input type="submit" name="ok" value="Envoyer">
                        <input type="reset" value="Réinitialiser">
                    </td>
                </tr>
            </table>';
    echo '</section>';
}

if ($_POST['titre']!=NULL) {

    if ($_POST['supp']==1) {
        $S3 = 'DELETE FROM article WHERE arID = '.$_GET['id'];
        $R3 = mysqli_query($B,$S3) or eh_bd_erreur($B,$S3);
        $T3 = mysqli_fetch_assoc($R3);
    }else{
        $time = getdate();

        $S3 = 'UPDATE article SET arTitre=\''.$_POST['titre'].'\' , arResume=\''.$_POST['resume'].'\', arTexte=\''.$_POST['titre'].'\', arDateModification='.$time['year'].zero($time['mon']).zero($time['mday']).zero($time['hours']).zero($time['minutes']).' WHERE arID = \''.$_GET['id'].'\'';

        $R3 = mysqli_query($B,$S3) or eh_bd_erreur($B,$S3);
        $T3 = mysqli_fetch_assoc($R3);
    }
}
eh_Fin();

?>
