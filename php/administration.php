<?php 
ob_start();
error_reporting(E_ALL);
session_start();

include 'bibli_gazette.php';
include 'bibli_generale.php';

if ($_SESSION['statut']!=2 && $_SESSION['statut']!=3) {
    header("Location: ../index.php");
    die();
}

foreach ($_POST as &$value) {
    $value = htmlspecialchars(htmlentities($value));
}

eh_toutDebut('../styles/gazette.css');
eh_afficherDebut("Admin","..");

echo '<section>
		<h2>Profil</h2>';

if ($_POST['pseudo']!=NULL) {
	$B=eh_bd_connecter();

	echo '<p>Pseudo : ',$_POST['pseudo'],'</p>';

	$S = 'SELECT COUNT(*) FROM commentaire WHERE coAuteur=\''.$_POST['pseudo'].'\'';
	$R = mysqli_query($B, $S);
    $T = mysqli_fetch_assoc($R);

	echo '<p>Nombre de commentaires publiés : ',$T['COUNT(*)'],'</p>';

	$S1 = 'SELECT COUNT(*) FROM article WHERE arAuteur=\''.$_POST['pseudo'].'\'';
	$R1 = mysqli_query($B, $S1);
    $T1 = mysqli_fetch_assoc($R1);

	echo '<p>Nombre d\'articles publiés : ',$T1['COUNT(*)'],'</p>';

	$S2 = 'SELECT COUNT(*) FROM commentaire,article WHERE coArticle=arID AND arAuteur=\''.$_POST['pseudo'].'\'';
	$R2 = mysqli_query($B, $S2);
    $T2 = mysqli_fetch_assoc($R2);

    $add=0;
    foreach ($T2 as &$value) {
    	$add = $add + $value;
	}
	$add=$add/$T1['COUNT(*)'];
	echo '<p>Nombre moyen de commentaires portant sur les articles qu\'il a publié : ',$add,'</p>';

}else{
	echo'<form action="../php/administration.php" method="post">
        <table>
            <tr>
                <td><label for="titre">Choisissez un pseudo :</label></td>
                <td><input type="text" name="pseudo" id="pseudo" value=""></td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="submit" name="ok" value="Envoyer">
                    <input type="reset" value="Réinitialiser">
                </td>
            </tr>
        </table>';
}
echo '</section>';

eh_Fin();
?>