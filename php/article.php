<?php 
ob_start();
error_reporting(E_ALL);
session_start();

include 'bibli_gazette.php';
include 'bibli_generale.php';

$B=eh_bd_connecter();

$id = (int) $_GET['id']; 

$pseudo = htmlentities($_POST['utPseudo']);
$com = htmlspecialchars(htmlentities($_POST['txtRemarques']));

if ($com != NULL) {
    $S = "SELECT coID FROM commentaire ORDER BY coID DESC";
    $R = mysqli_query($B, $S);
    $T = mysqli_fetch_assoc($R);
    $coid = $T['coID']+1;
    $time = getdate();
    $S3 = 'INSERT INTO commentaire (coID,coAuteur,coTexte,coDate,coArticle) VALUES ('.$coid.',\''.$_SESSION['pseudo'].'\',\''.$com.'\','.$time['year'].zero($time['mon']).zero($time['mday']).zero($time['hours']).zero($time['minutes']).','.$id.')';
    $R3 = mysqli_query($B,$S3) or eh_bd_erreur($B,$S3);
    $T3 = mysqli_fetch_assoc($R3);
}


$S = 'SELECT * FROM article WHERE arID = '.$id;
$R = mysqli_query($B,$S) or eh_bd_erreur($B,$S);
$T = mysqli_fetch_assoc($R);


eh_toutDebut('../styles/gazette.css');

$titre='L\'actu';
eh_afficherDebut($titre,"..");

echo '<article>
            <h3>',htmlentities($T['arTitre']),'</h3>
            <img src="../images/';
    eh_image($id);
        echo '"  width="250" height="187" alt="',htmlentities($T['arTitre']),'">';

echo htmlentities($T['arTexte']);

if(htmlentities($t['arDateModification'])!=NULL){
        echo '<footer>Par <a href="redaction.html#',htmlentities($T['arAuteur']),'">',htmlentities($T['arAuteur']),'</a>. Modifié le ',htmlentities($T['arDateModification'][6]),htmlentities($T['arDateModification'][7]);
        eh_mois($T['arDateModification']);
        echo htmlentities($T['arDateModification'][0]),htmlentities($T['arDateModification'][1]),htmlentities($T['arDateModification'][2]),htmlentities($T['arDateModification'][3]),' à ',htmlentities($T['arDateModification'][8]),htmlentities($T['arDateModification'][9]),':',htmlentities($T['arDateModification'][10]),htmlentities($T['arDateModification'][11]),'</footer>';
    }
    else{
        echo '<footer>Par <a href="redaction.html#',htmlentities($T['arAuteur']),'">',htmlentities($T['arAuteur']),'</a>. Publié le ',htmlentities($T['arDatePublication'][6]),htmlentities($T['arDatePublication'][7]);
        eh_mois($T['arDatePublication']);
        echo htmlentities($T['arDatePublication'][0]),htmlentities($T['arDatePublication'][1]),htmlentities($T['arDatePublication'][2]),htmlentities($T['arDatePublication'][3]),' à ',htmlentities($T['arDatePublication'][8]),htmlentities($T['arDatePublication'][9]),':',htmlentities($T['arDatePublication'][10]),htmlentities($T['arDatePublication'][11]),'</footer>';
    }



//-------------------------------------------------------

$S1 = "SELECT * FROM commentaire WHERE coArticle = $id ORDER BY coID DESC";
$R1 = mysqli_query($B, $S1);
$T1 = mysqli_fetch_assoc($R1);

$cpt=$T1[coID];



echo '</article>
        <section>
            <h2>Réactions</h2>
            <ul>';

while ($cpt !=0) {
    if ($T1 != NULL) {
        eh_afficherCom($T1);
    }
	$cpt--;
	$S1 = "SELECT * FROM commentaire WHERE coArticle = $id AND coID=$cpt";
	$R1 = mysqli_query($B, $S1);
	$T1 = mysqli_fetch_assoc($R1);
	
}
    
echo '</ul>';
if ($_SESSION != NULL) {
    echo '<p>Ajouter un commentaire</p>
            <form action="./article.php?id=',$id,'" method="post">
                <textarea name="txtRemarques" rows="20" cols="100"></textarea><br>
                <input type="submit" name="btnEnvoi" value="Publier ce commentaire">
            </form>';
}else{
echo '      <p>
                <a href="../php/connexion.php">Connectez-vous</a> ou <a href="./inscription.html">inscrivez-vous</a> pour pouvoir commenter cet article !
            </p>
            
        </section>';
} 
eh_Fin();


?>
