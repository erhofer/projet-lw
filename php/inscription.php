<?php 
ob_start();
error_reporting(E_ALL);
include 'bibli_gazette.php';
include 'bibli_generale.php';

eh_toutDebut('../styles/gazette.css');

$titre='Inscription';
eh_afficherDebut($titre,"..");

$inscription = $_POST;

$B=eh_bd_connecter();

foreach ($inscription as &$value) {
    $value = htmlentities(htmlspecialchars($value));
}

echo '<section>
            <h2>Formulaire d\'inscription</h2><h2>Formulaire d\'inscription</h2>
            <p>Pour vous inscrire, remplissez le formulaire ci-dessous.</p>';


$errs = ehl_traitement_inscription($inscription,$B,1);

$ok = ehl_aff_formulaire($inscription,$B,$errs);



if ($ok == 1) {
    session_start();
    if (isset($_POST['pseudo']) AND !empty($_POST['pseudo'])) {
        $_SESSION = $_POST;
        $_SESSION['pseudo'] = $_POST['pseudo'];
        echo '<p>',$_SESSION['pseudo'],'</p>';
    }

    header("Location: ./protegee.php");
    die();
}
eh_Fin();
?>