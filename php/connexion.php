<?php 
ob_start();
error_reporting(E_ALL);
include 'bibli_gazette.php';
include 'bibli_generale.php';

eh_toutDebut('../styles/gazette.css');

$titre='Connexion';
eh_afficherDebut($titre,"..");

$connexion = $_POST;

$B=eh_bd_connecter();

foreach ($connexion as &$value) {
    $value = htmlspecialchars($value);
}

$ok = '';

if (isset($_POST['pseudo']) AND !empty($_POST['pseudo'])) {
    $S = 'SELECT utPseudo FROM utilisateur WHERE utPseudo = \''.$connexion['pseudo'].'\'';
    $R = mysqli_query($B,$S) or eh_bd_erreur($B,$S);
    $T = mysqli_fetch_assoc($R);
    if ($T != NULL) {
        $S1 = 'SELECT utPasse,utStatut FROM utilisateur WHERE utPseudo = \''.$connexion['pseudo'].'\'';
        $R1 = mysqli_query($B,$S1) or eh_bd_erreur($B,$S1);
        $T1 = mysqli_fetch_assoc($R1);
        if (password_verify($connexion['mdp'],$T1['utPasse'])) {
            session_start();
            $_SESSION = $_POST;
            $_SESSION['pseudo'] = $connexion['pseudo'];
            $_SESSION['statut'] = $T1['utStatut'];

            echo '<p>',$_SESSION['pseudo'],'</p>';
            header("Location: ./protegee.php");
            die();
        }else{
            $ok = '<p>Echec d\'authentification. Mot de passe incorrect.</p>';
        }
    }else{
        $ok = '<p>Echec d\'authentification. Utilisateur inconnu.</p>';
    }
}

echo '<section>
            <h2>Formulaire de connexion</h2><h2>Formulaire de connexion</h2>
            <p>Pour vous identifier, remplissez le formulaire ci-dessous.</p>';
echo $ok;
echo '<form action="../php/connexion.php" method="post"><table><tr>';
eh_aff_ligne_input("Pseudo : ","pseudo","pseudo","text");
echo '</tr><tr>';
eh_aff_ligne_input("Mot de passe : ","mdp","mdp","password");
echo '</tr>';
echo '<tr>
        <td colspan="2">
            <input type="submit" name="btnInscription" value="Se connecter">
            <input type="reset" value="Réinitialiser">
        </td>
    </tr>
</table>';
echo 'Pas encore inscrit ? N\'attendez pas <a href="./inscription">inscrivez-vous</a> !';
echo '</section>';

eh_Fin();
?>