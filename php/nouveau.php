<?php 
ob_start();
error_reporting(E_ALL);
session_start();

include 'bibli_gazette.php';
include 'bibli_generale.php';

if ($_SESSION['statut']!=1 && $_SESSION['statut']!=2 && $_SESSION['statut']!=3) {
    header("Location: ../index.php");
    die();
}

$B=eh_bd_connecter();

foreach ($_POST as &$value) {
    $value = htmlspecialchars(htmlentities($value));
}

if ($_POST['titre']!=NULL) {
    $S = "SELECT arID FROM article ORDER BY arID DESC";
    $R = mysqli_query($B, $S);
    $T = mysqli_fetch_assoc($R);

    $arid = $T['arID']+1;

    $time = getdate();

    $S3 = 'INSERT INTO article (arID,arTitre,arResume,arTexte,arDatePublication,arDateModification,arAuteur) 
    VALUES ('.$arid.',
    \''.$_POST['titre'].'\',
    \''.$_POST['resume'].'\',
    \''.$_POST['texte'].'\',
    '.$time['year'].zero($time['mon']).zero($time['mday']).zero($time['hours']).zero($time['minutes']).',NULL,\''.$_SESSION['pseudo'].'\')';

    $R3 = mysqli_query($B,$S3) or eh_bd_erreur($B,$S3);
    $T3 = mysqli_fetch_assoc($R3);

    header('Location: ./article.php?id='.$arid);
    die();
}else{
    echo 'ERREUR';
}

eh_toutDebut('../styles/gazette.css');
eh_afficherDebut("Nouvel Article","..");
echo '<section><h2>Nouvel Article</h2>';
echo'<form action="../php/nouveau.php" method="post">
                <table>
                    <tr>
                        <td><label for="titre">Choisissez un titre :</label></td>
                        <td><input type="text" name="titre" id="titre" value=""></td>
                    </tr>
                    <tr>
                        <td><label for="resume">Quel est le résumé :</label></td>
                        <td><textarea name="resume" rows="10" cols="80"></textarea></td>
                    </tr>
                    <tr>
                        <td><label for="texte">Votre texte :</label></td>
                        <td><textarea name="texte" rows="30" cols="100"></textarea></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <input type="submit" name="ok" value="Envoyer">
                            <input type="reset" value="Réinitialiser">
                        </td>
                    </tr>
                </table>';
        echo '</section>';
eh_Fin();

?>
