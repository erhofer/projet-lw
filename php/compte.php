<?php 
ob_start();
error_reporting(E_ALL);
session_start();
include 'bibli_gazette.php';
include 'bibli_generale.php';

eh_toutDebut('../styles/gazette.css');

$B=eh_bd_connecter();

$verif = $_POST;

foreach ($verif as &$value) {
    $value = htmlentities(htmlspecialchars($value));
}

if ($verif['cbSpam'] != NULL) {
    $errs = ehl_traitement_inscription($verif,$B,0);
}else{
    if (strcmp($verif['passe1'],$verif['passe2'])!=0) {
        $err['passe1'] = 'Les mots de passe doivent être identiques.';
    }
 }



//Affichage informations dans formulaire
$S = 'SELECT * FROM utilisateur WHERE utPseudo = \''.$_SESSION['pseudo'].'\'';
$R = mysqli_query($B,$S) or eh_bd_erreur($B,$S);
$connexion = mysqli_fetch_assoc($R);


//----

eh_afficherDebut('Mon compte',"..");

echo '<section>
            <h2>Information personnelles</h2>
            <p>Vous pouvez modifier les information suivantes.</p>';

//Affichage erreurs 1
if ($errs!=NULL){
        echo "<p>Les erreurs suivantes ont été relevées lors de vos modifications :</p>";
        echo "<ul>";
        foreach ( $errs as &$erreur) {
            echo "<li>",$erreur,"</li>";
        }
        echo "</ul>";
}else{
    if($verif['cbSpam'] != NULL) {
        $S1 = 'UPDATE utilisateur SET utNom = \''.$verif['nom'].'\', utPrenom = \''.$verif['prenom'].'\',utEmail = \''.$verif['email'].'\',utMailsPourris = \''.$verif['cbSpam'].'\',utCivilite = \''.$verif['radSexe'].'\', WHERE utPseudo = \''.$_SESSION['pseudo'].'\'';
        $R1 = mysqli_query($B,$S1) or eh_bd_erreur($B,$S1);
        $T1 = mysqli_fetch_assoc($R1);
    }
}
//-----

echo '<form action="../php/compte.php" method="post">
    <table>';

if ($connexion['utCivilite']== 'h' ) {
    echo '                  <tr>
                        <td>Votre civilité :</td>
                        <td>
                            <label><input type="radio" name="radSexe" id="radio1" value="h" selected> Monsieur</label>
                            <input type="radio" name="radSexe" id="radio2" value="f"> <label for="radio2">Madame</label>
                            
                        </td>
                    </tr>';
}else{
    echo '                  <tr>
                        <td>Votre civilité :</td>
                        <td>
                            <label><input type="radio" name="radSexe" id="radio1" value="h"> Monsieur</label>
                            <input type="radio" name="radSexe" id="radio2" value="f" selected> <label for="radio2">Madame</label>
                            
                        </td>
                    </tr>';
}
echo '                  <tr>
                        <td><label for="txtNom">Votre nom :</label></td>
                        <td><input type="text" name="nom" id="txtNom" value="',$connexion['utPseudo'],'"></td>
                    </tr>
                    <tr>
                        <td><label for="txtPrenom">Votre prénom :</label></td>
                        <td><input type="text" name="prenom" id="txtPrenom" value="',$connexion['utPrenom'],'"></td>
                    </tr>                 
                    <tr>
                        <td>Votre date de naissance :</td>
                        <td>';
                        eh_aff_liste_nombre('jour',1,31,1,1,$T1['coDate'][7]+$T1['coDate'][6]*10);
                        eh_aff_liste_mois('mois');
                        eh_aff_liste_nombre('annee',1945,2020,-1,1);
                        echo'</td>
                    </tr>
                    <tr>
                        <td><label for="txtEmail">Votre email :</label></td>
                        <td><input type="text" name="email" id="txtEmail" value="',$connexion['utEmail'],'"></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <label><input type="checkbox" name="cbSpam" value="1" checked>
                                J\'accepte de recevoir des tonnes de mails pourris </label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <input type="submit" name="btnInscription" value="Enregistrer">
                            <input type="reset" value="Réinitialiser">
                        </td>
                    </tr>
        </table></form>
    </section>';



echo '<section>
            <h2>Authentification</h2>
            <p>Vous pouvez modifier votre mot de passe ci-dessous.</p>';

//Affichage erreurs 2
if ($err!=NULL){
        echo "<p>Les erreurs suivantes ont été relevées lors de votre changement de mot de passe :</p>";
        echo "<ul>";
        foreach ( $err as &$erreur) {
            echo "<li>",$erreur,"</li>";
        }
        echo "</ul>";
}else{
    if ($verif['passe1']!=NULL) {
        $S2 = 'UPDATE utilisateur SET utPasse = \''.password_hash($verif['passe1'],PASSWORD_DEFAULT).'\' WHERE utPseudo = \''.$_SESSION['pseudo'].'\'';
        $R2 = mysqli_query($B,$S2) or eh_bd_erreur($B,$S2);
        $T2 = mysqli_fetch_assoc($R2);
    }
}
//----

echo '<form action="../php/compte.php" method="post">
    <table>
        <tr>
            <td><label for="txtPassword1">Choisissez un mot de passe :</label></td>
            <td><input type="password" name="passe1" id="txtPassword1"></td>
        </tr>
            <tr>
                <td><label for="txtPassword2">Répétez le mot de passe :</label></td>
                <td><input type="password" name="passe2" id="txtPassword2"></td>
            </tr>
            <tr>
                <td colspan="2">
                   <input type="submit" name="btnInscription" value="Enregistrer">
                </td>
            </tr>
        </table>
    </form>
</section>';


eh_Fin();
?>