<?php
    define(BD_SERVER , "localhost"); 
    define(BD_NAME , "gazette_bd"); 
    define(BD_USER, "gazette_user");
    define(BD_PASS, "gazette_pass");

    function eh_block($id,$texte,$image,$titre,$class){
        echo '<section class="centre">';
        echo '<h2>&Agrave; la Une</h2>';
    }
    
    class Article {
        private $id;
        private $texte;
        private $image;

        public function __construct($p1, $p2, $p3) {
            $this->id = $p1;
            $this->texte = $p2;
            $this->image = $p3;
        }
        
         public function load($p1, $p2, $p3) { 
            $this->id = $p1;
            $this->texte = $p2;
            $this->image = $p3;
        }
    
        public function afficher() {
            echo '</a>';
            echo '<a href="./php/article.php?id=',$this->id,'">';
            echo '<img src="images/',$this->image,'" alt="',$this->texte,'"><br>';
            echo $this->texte;
            echo '</a>';
        }
    }
    
    class Block {
        private $titre;
        private $classe;
        
        public function __construct($p1, $p2) {
            $this->titre = $p1;
            $this->classe = $p2;
        }
        
        public function load($p1, $p2) {
            $this->titre = $p1;
            $this->classe = $p2;
        }
        
        public function afficher($a1,$a2,$a3){
            echo '<section class="',$this->classe,'">','<h2>',$this->titre,'</h2>';
            $a1->afficher();
            $a2->afficher();
            $a3->afficher();
            echo '</section>';
        }
    }
    
    
    function eh_afficherDebut($titre,$chemin){
        echo '<body>
    <nav>
        <ul>
            <li><a href="',$chemin,'/index.php">Accueil</a></li>
            <li><a href="',$chemin,'/php/actus.php">Toute l\'actu</a></li>
            <li><a href="',$chemin,'/php/recherche.php">Recherche</a></li>
            <li><a href="',$chemin,'/html/redaction.html">La rédac\'</a></li>
            <li>';
if ($_SESSION['pseudo'] == NULL) {
    echo '<a href="',$chemin,'/php/connexion.php">Se connecter</a>';
}else{
    echo '<a href="#">',$_SESSION['pseudo'],'</a><ul>'
        , '<li><a href="',$chemin,'/php/compte.php">Mon profil</a></li>';
        if($_SESSION['statut'] > 0){
            echo '<li><a href="',$chemin,'/php/nouveau.php">Nouvel article</a></li>';
        }
        echo '<li><a href="',$chemin,'/php/deconnexion.php">Se déconnecter</a></li>'
        , '</ul>';
}
            
            echo '</li>
        </ul>
    </nav>
    <header>
        <img src="',$chemin,'/images/titre.png" alt="La gazette de L-INFO" width="780" height="83">
        <h1>',$titre,'</h1>
    </header>
    <main>';
    }
    
    function eh_Fin() {
        echo '</main>'
        , '<footer>&copy; Licence Informatique - Janvier 2020 - Tous droits réservés</footer>'
        , '</body>';
        echo '</html>';
       
    }
    
    function eh_astro() {
        echo '<section>';
        echo '<h2>Horoscope de la semaine</h2>';
        echo '<p>Vous l\'attendiez tous, voici l\'horoscope du semestre pair de l\'année 2019-2020. Sans surprise, il n\'est pas terrible...</p>';
        echo '<table id="horoscope">';
        echo '<tr>';
        echo '<td>Signe</td>';
        echo '<td>Date</td>';
        echo '<td>Votre horoscope</td>';
        echo '</tr>';
        echo '<tr>';
        echo '<td>&#9800; Bélier</td>';
        echo '<td>du 21 mars<br>au 19 avril</td>';
        echo '<td rowspan="4">';
        echo '<p>Après des vacances bien méritées, l\'année reprend sur les chapeaux de roues. Tous les signes sont concernés. </p>';
        echo '<p>Jupiter s\'aligne avec Saturne, péremptoirement à Venus, et nous promet un semestre qui ne sera pas de tout repos. Février sera le mois le plus tranquille puisqu\'il ne comporte que 29 jours.</p>';
        echo '<p>Les fins de mois seront douloureuses pour les natifs du 2e décan au moment où tomberont les tant-attendus résultats';
        echo 'du module d\'<em>Algorithmique et Structures de Données</em> du semestre 3.</p>';
        echo '</td>';
        echo '</tr>';
        echo '<tr>';
        echo '<td>&#9801; Taureau</td>';
        echo '<td>du 20 avril<br>au 20 mai</td>';
        echo '</tr>';
        echo '<tr>';
        echo '<td>...</td>';
        echo '<td>...</td>';
        echo '</tr>';
        echo '<tr>';
        echo '<td>&#9811; Poisson</td>';
        echo '<td>du 20 février<br>au 20 mars</td>';
        echo '</tr>';
        echo '</table>';
        echo '<p>Malgré cela, notre équipe d\'astrologues de choc vous souhaite à tous un bon semestre, et bon courage pour le module de';
        echo '<em>Système et Programmation Système</em>.</p>';
        echo '</section>';
    }

    function eh_afficherCom($T1){
        echo '<li>
                    <p>Commentaire de <strong>',$T1['coAuteur'],'</strong>, le ',$T1['coDate'][6],$T1['coDate'][7];
        eh_mois($T1['coDate']);
        echo $T1['coDate'][0],$T1['coDate'][1],$T1['coDate'][2],$T1['coDate'][3],' à ',$T1['coDate'][8],$T1['coDate'][9],':',$T1['coDate'][10],$T1['coDate'][11],'</p>
                    <blockquote>',$T1['coTexte'],'</blockquote>
                </li>';
    }

    function eh_mois($tab){
        if ($tab[4]==0) {
            switch($tab[5]) {
                case 1:
                    echo ' janvier ';
                    break;
                case 2:
                    echo ' février ';
                    break;
                case 3:
                    echo ' mars ';
                    break;
                case 4:
                    echo ' avril ';
                    break;
                case 5:
                    echo ' mai ';
                    break;
                case 6:
                    echo ' juin ';
                    break;
                case 7:
                    echo ' juillet ';
                    break;
                case 8:
                    echo ' août ';
                    break;
                case 9:
                    echo ' septembre ';
                    break;
                default:
                    echo 'aye';
                    break;
            }
        }else {
            switch ($tab[5]) {
                case 0:
                    echo ' octobre ';
                    break;
                case 1:
                    echo' novembre ';
                    break;
                case 2:
                    echo ' décembre ';
                    break;
                default:
                    echo 'aye';
                    break;
            }
        }
    }


function  ehl_traitement_inscription($inscription,$B,$from){
        $timestamp=getdate();

    $nb = strlen($inscription['pseudo']);

    if ($nb < 4 && $from == 1) {
        $errs['pseudo'] = 'Le pseudo doit avoir 4 caractères minimum.';
    }

    if ($inscription['radSexe']==NULL) {
        $errs['radSexe'] = 'Vous devez choisir une civilité.';
    }

    if (strcmp($inscription['passe1'],$inscription['passe2'])!=0) {
        $errs['passe1'] = 'Les mots de passe doivent être identiques.';
    }

    if ($inscription['email']==NULL) {
        $errs['email'] = 'L\'adresse email ne doit pas être vide.';
    }

    if ($timestamp['year']-$inscription['annee']<18) {
        $errs['annee'] = 'Vous devez avoir au moins avoir 18 ans pour vous inscrire.';
    }elseif ($timestamp['year']-$inscription['annee']==18) {
        if ($timestamp['mon']-$inscription['mois']<0) {
            $errs['annee'] = 'Vous devez avoir au moins avoir 18 ans pour vous inscrire.';
        }elseif ($timestamp['mon']-$inscription['mois']==0) {
            if ($timestamp['mday']-$inscription['jour']<0) {
                $errs['annee'] = 'Vous devez avoir au moins avoir 18 ans pour vous inscrire.';
            }
        }
    }

    $S = 'SELECT utPseudo FROM utilisateur';

    if($errs==NULL){
        $R = mysqli_query($B,$S) or eh_bd_erreur($B,$S);
        $T = mysqli_fetch_assoc($R);
        foreach ($T as &$key) {
            if ($key == $inscription['pseudo']) {
                $errs['pseudo'] = 'Le pseudo existe déjà.';
            }
        }
    }
    return $errs;
}

function ehl_aff_formulaire($inscription,$B,$errs){
    $S1="fd";
    if($errs==NULL){
        $age = $inscription['annee'].$inscription['mois'].$inscription['jour'];
        echo '<p>Aucune erreur de saisie !</p>';

        $S1 = 'INSERT INTO utilisateur (utPseudo,utNom,utPrenom,utEmail,utPasse,utDateNaissance,utStatut,utCivilite,utMailsPourris) VALUES (\''.$inscription['pseudo'].'\',\''.$inscription['nom'].'\',\''.$inscription['prenom'].'\',\''.$inscription['email'].'\',\''.password_hash($inscription['passe1'],PASSWORD_DEFAULT).'\',\''.$age.'\',0,\''.$inscription['radSexe'].'\',\''.$inscription['cbCGU'].'\')';
        $R1 = mysqli_query($B,$S1) or eh_bd_erreur($B,$S1);
        $T1 = mysqli_fetch_assoc($R1);

        return 1;

    }else{
        echo "<p>Les erreurs suivantes ont été relevées lors de votre inscription :</p>";
        echo "<ul>";
        foreach ( $errs as &$erreur) {
            echo "<li>",$erreur,"</li>";
        }
        echo "</ul>";
    }
    echo'<form action="../php/inscription.php" method="post">
                <table>
                    <tr>
                        <td><label for="txtPseudo">Choisissez un pseudo :</label></td>
                        <td><input type="text" name="pseudo" id="txtPseudo" value="',$inscription['pseudo'],'" placeholder="6 caractères minimum"></td>
                    </tr>
                    <tr>
                        <td>Votre civilité :</td>
                        <td>
                            <label><input type="radio" name="radSexe" id="radio1" value="h"> Monsieur</label>
                            <input type="radio" name="radSexe" id="radio2" value="f"> <label for="radio2">Madame</label>
                            
                        </td>
                    </tr>
                    <tr>
                        <td><label for="txtNom">Votre nom :</label></td>
                        <td><input type="text" name="nom" id="txtNom" value="',$inscription['nom'],'"></td>
                    </tr>
                    <tr>
                        <td><label for="txtPrenom">Votre prénom :</label></td>
                        <td><input type="text" name="prenom" id="txtPrenom" value="',$inscription['prenom'],'"></td>
                    </tr>
                    <tr>
                        <td>Votre date de naissance :</td>
                        <td>';
                        eh_aff_liste_nombre('jour',1,31,1,1);
                        eh_aff_liste_mois('mois');
                        eh_aff_liste_nombre('annee',1945,2020,-1,1);
                        echo'</td>
                    </tr>
                    <tr>
                        <td><label for="txtEmail">Votre email :</label></td>
                        <td><input type="text" name="email" id="txtEmail" value="',$inscription['email'],'"></td>
                    </tr>
                    <tr>
                        <td><label for="txtPassword1">Choisissez un mot de passe :</label></td>
                        <td><input type="password" name="passe1" id="txtPassword1"></td>
                    </tr>
                    <tr>
                        <td><label for="txtPassword2">Répétez le mot de passe :</label></td>
                        <td><input type="password" name="passe2" id="txtPassword2"></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <label><input type="checkbox" name="cbCGU" value="1" required>
                                J\'ai lu et j\'accepte les conditions générales d\'utilisation </label>
                            <label><input type="checkbox" name="cbSpam" value="1" checked>
                                J\'accepte de recevoir des tonnes de mails pourris </label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <input type="submit" name="btnInscription" value="S\'inscrire">
                            <input type="reset" value="Réinitialiser">
                        </td>
                    </tr>
                </table>';
        echo '</section>';
}


function eh_aff_liste_nombre($name,$min,$max,$ite,$default){

    echo '<select name="',$name,'">';

    for ($i=$min; $i <= $max; $i++) { 
        if (i<10) {
            if (i==$default) {
                 echo '<option value="0',$i,'" selected>',$i,'</option>';
            }else{
                echo '<option value="0',$i,'">',$i,'</option>';
            }
        }else{
            if (i==$default) {
                echo '<option value="',$i,'" selected>',$i,'</option>';
            }else{
                echo '<option value="',$i,'">',$i,'</option>';
            }
        }
    }
    echo '</select>';

}

function eh_aff_liste_mois($name){
    echo '<select name="',$name,'"><option value="01" selected>janvier</option><option value="02">février</option><option value="03">mars</option><option value="04">avril</option><option value="05">mai</option><option value="06">juin</option><option value="07">juillet</option><option value="08">août</option><option value="09">septembre</option><option value="10">octobre</option><option value="11">novembre</option><option value="12">décembre</option></select>';
}

function eh_session_exit($s){
    session_destroy();
    session_unset();
    header("Location: $s/index.php");
    die();
}

function eh_verifie_authentification(){
    if ($_SESSION['pseudo'] == NULL) {
        return 1;
    }else{
        return 0;
    }
}

function eh_aff_input_checkbox($txt,$name,$val){
    echo '<label><input type="checkbox" name="',$name,'" value="',$val,'">',$txt,'</label>';
}

function eh_aff_ligne_input($txt,$name,$val,$type){
    echo '<td><label for="',$name,'">',$txt,'</label></td>
          <td><input type="',$type,'" name="',$val,'" id="',$name,'"></td>';
}

function eh_aff_input_radio($txt,$name,$val,$id){
    echo '<label><input type="radio" name="',$name,'" id="',$id,'" value="',$val,'"> ',$txt,'</label>';
}

function afficherArt($B,$T){
    //$S = 'SELECT * FROM `article` WHERE arID = '.$id;
    //$R = mysqli_query($B,$S) or eh_bd_erreur($B,$S);
    //$T = mysqli_fetch_assoc($R);

    echo '<article class="resume">
                <img src="../images/';
                eh_image($T['arID']);
    echo'" alt="Photo d\'illustration | ',$T['arTitre'],'">
                <h3>',$T['arTitre'],'</h3>
                <p>',$T['arResume'],'</p>
                <footer><a href="../php/article.php?id=',$T['arID'],'">Lire l\'article</a></footer>
            </article>';
}

function afficherGArt($id,$B,$max){
    $cpt = 1;

    $S = 'SELECT * FROM `article` WHERE arID = '.$max;
    $R = mysqli_query($B,$S) or eh_bd_erreur($B,$S);
    $T = mysqli_fetch_assoc($R);

    echo '<section>
            <h2>',eh_mois($T['arDatePublication']),' ',$T['arDatePublication'][0],$T['arDatePublication'][1],$T['arDatePublication'][2],$T['arDatePublication'][3],'</h2>';
            
    afficherArt($B,$T);


    $S1 = 'SELECT * FROM `article` WHERE arID = '.($max-1);
    $R1 = mysqli_query($B,$S1) or eh_bd_erreur($B,$S1);
    $T1 = mysqli_fetch_assoc($R1);

    $id++;

    if ($T['arDatePublication'][0] == $T1['arDatePublication'][0] && 
        $T['arDatePublication'][1] == $T1['arDatePublication'][1] && 
        $T['arDatePublication'][2] == $T1['arDatePublication'][2] && 
        $T['arDatePublication'][3] == $T1['arDatePublication'][3] && 
        $T['arDatePublication'][4] == $T1['arDatePublication'][4] && 
        $T['arDatePublication'][5] == $T1['arDatePublication'][5] && $id <4) {

        afficherArt($B,$T1);
        $cpt = 2;

        $S2 = 'SELECT * FROM `article` WHERE arID = '.($max-2);
        $R2 = mysqli_query($B,$S2) or eh_bd_erreur($B,$S2);
        $T2 = mysqli_fetch_assoc($R2);

        $id++;

        if ($T2['arDatePublication'][0] == $T1['arDatePublication'][0] && 
            $T2['arDatePublication'][1] == $T1['arDatePublication'][1] && 
            $T2['arDatePublication'][2] == $T1['arDatePublication'][2] && 
            $T2['arDatePublication'][3] == $T1['arDatePublication'][3] && 
            $T2['arDatePublication'][4] == $T1['arDatePublication'][4] && 
            $T2['arDatePublication'][5] == $T1['arDatePublication'][5] && $id <4) {

            afficherArt($B,$T2);
            $cpt = 3;
        }
    }

    echo '</section>';

    return $cpt;
}

function zero($chiffre){
    if ($chiffre<10) {
        return '0'.$chiffre;
    }else{
        return $chiffre;
    }
}

function section1($pseudo,$nom,$prenom,$resume){
    echo '<section>
            <h2>Notre rédacteur en chef</h2>
            <article class="redacteur" id="',$pseudo,'">
                <img src="../images/',$pseudo,'.jpg" width="150" height="200" alt="',$prenom,' ',$nom,'">
                <h3>',$prenom,' ',$nom,'</h3>
                ',$resume,'
            </article>
        </section>';
}

function eh_image($id){
    if ($id==10) {
        echo 'hacker.jpg';
    }elseif ($id==9) {
        echo 'hymne.jpg';
    }elseif ($id==8) {
        echo 'melenchon.jpg';
    }elseif ($id==1) {
        echo 'sondage.jpg';
    }elseif ($id==7) {
        echo 'pingouins.jpg';
    }elseif ($id==3) {
        echo 'macron.jpg';
    }elseif ($id==2) {
        echo 'walkingdead.jpg';
    }elseif ($id==5) {
        echo 'calendrier.jpg';
    }elseif ($id==4) {
        echo 'trump.jpg';
    }elseif ($id==6) {
        echo 'arnaque.jpg';
    }
}

?>
