<?php 
ob_start();
error_reporting(E_ALL);
session_start();

include 'bibli_gazette.php';
include 'bibli_generale.php';


eh_toutDebut('../styles/gazette.css');

$titre='Page accessible uniquement aux utilisateurs authentifiés';
eh_afficherDebut($titre,"..");



$B=eh_bd_connecter();
$S = 'SELECT * FROM utilisateur WHERE utPseudo = \''.$_SESSION['pseudo'].'\'';
$R = mysqli_query($B,$S) or eh_bd_erreur($B,$S);
$T = mysqli_fetch_assoc($R);


echo '<section>
            <h2>Utilisateur : ',$_SESSION['pseudo'],'</h2>
            <p>SID : ',session_id(),'</p>
            <h3>Données mémorisées dans la table Utilisateur</h3>';

echo '<ul>';
    echo '<li>utPseudo : ',$T['utPseudo'],'</li>';
    echo '<li>utNom : ',$T['utNom'],'</li>';
    echo '<li>utPrenom : ',$T['utPrenom'],'</li>';
    echo '<li>utEmail : ',$T['utEmail'],'</li>';
    echo '<li>utPasse : ',$T['utPasse'],'</li>';
    echo '<li>utDateNaissance : ',$T['utDateNaissance'],'</li>';
    echo '<li>utStatut : ',$T['utStatut'],'</li>';
    echo '<li>utCivilite : ',$T['utCivilite'],'</li>';
    echo '<li>utMailsPourris : ',$T['utMailsPourris'],'</li>';
echo '</ul>';

eh_Fin();

?>