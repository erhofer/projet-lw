<?php 
ob_start();
error_reporting(E_ALL);
session_start();

include 'bibli_gazette.php';
include 'bibli_generale.php';

if ($_GET['recherche'] != NULL) {
	$B = eh_bd_connecter();
	$S = 'SELECT * FROM article WHERE arTitre LIKE "%'.$_GET['recherche'].'%" ORDER BY arID DESC';
	$R = mysqli_query($B,$S) or eh_bd_erreur($B,$S);
	$T = mysqli_fetch_assoc($R);
}

$cpt=$T['arID'];

eh_toutDebut('../styles/gazette.css');

eh_afficherDebut("Recherche","..");

echo '<section>
	<h2>Recherche des articles</h2>
    <p>Les critères de recherche doivent faire au moins 3 caractères pour être pris en compte.</p>
    <form action="../php/recherche.php" method="get">
		<input type="text" name="recherche" id="recherche">
		<input type="submit" name="btnInscription" value="Rechercher">
    </section>';

/*while ($cpt !=0) {
    if ($T1 != NULL) {
        eh_afficherCom($T1);
    }
	$cpt--;
	$S1 = "SELECT * FROM commentaire WHERE coArticle = $id AND coID=$cpt";
	$R1 = mysqli_query($B, $S1);
	$T1 = mysqli_fetch_assoc($R1);
	
}*/
while ($cpt >0) {
	if ($T['arID'] != NULL) {
		echo '<section>
        	<h2>',eh_mois($T['arDatePublication']),' ',$T['arDatePublication'][0],$T['arDatePublication'][1],$T['arDatePublication'][2],$T['arDatePublication'][3],'</h2>';
 		afficherArt($B,$T);
 		echo '</section>';
 	}
 	$cpt--;
	$S = 'SELECT * FROM article WHERE arTitre LIKE "%'.$_GET['recherche'].'%" AND arID ='.$cpt;
	$R = mysqli_query($B,$S) or eh_bd_erreur($B,$S);
	$T = mysqli_fetch_assoc($R);
}


eh_Fin();
?>